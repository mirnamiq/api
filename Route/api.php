<?php

use App\Controller\UserController;
use App\Library\Route;

Route::get('/user/get', [UserController::class, 'index']);
Route::post('/user/store', [UserController::class, 'store']);