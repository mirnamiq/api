<?php

require_once 'config.php';

spl_autoload_register(function ($class) {
    require_once BASE_DIR.str_replace('\\', '/', $class).'.php';
});

require_once BASE_DIR.'Route/api.php';

