<?php

namespace App\Model;

use App\Library\DB;

class Model implements ModelInterface
{
    /**
     * @var string
     */
    protected string $table;

    /**
     * @param string ...$columns
     * @return bool|array
     */
    public function get(string ...$columns): bool|array
    {
        if(count($columns) === 0) $columns = ['*'];
        return (new DB())->query("SELECT ".implode(', ', $columns)." FROM ". $this->table)
            ->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param array $columns
     * @return void
     */
    public function create(array $columns)
    {
        $questions = implode(', ',array_fill(0, count($columns), '?'));
        $keys = implode(', ', array_keys($columns));
        $stmt = (new DB())->prepare("INSERT INTO ".$this->table." (".$keys.") VALUES (".$questions.")");
        $stmt->execute(array_values($columns));
    }
}