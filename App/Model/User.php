<?php

namespace App\Model;

class User extends Model
{
    /**
     * @var string
     */
    public string $table = 'users';
}