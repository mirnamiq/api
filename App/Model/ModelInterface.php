<?php
namespace App\Model;

interface ModelInterface
{
    /**
     * @param array $columns
     * @return void
     */
    public function create(array $columns);

    /**
     * @return bool|array
     */
    public function get(): bool|array;
}