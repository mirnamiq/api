<?php

namespace App\Controller;

use JetBrains\PhpStorm\NoReturn;

class Controller
{
    /**
     * __call magic method.
     */
    #[NoReturn]
    public function __call($name, $arguments)
    {
        $this->response('', array('HTTP/1.1 404 Not Found'));
    }

    /**
     * @param mixed $data
     * @param array $httpHeaders
     * @return mixed
     */
    protected function response(mixed $data, array $httpHeaders = []): mixed
    {
        header_remove('Set-Cookie');

        if (count($httpHeaders)) {
            foreach ($httpHeaders as $httpHeader) {
                header($httpHeader);
            }
        }

        return $data;
    }

    /**
     * @param mixed $data
     * @param array $httpHeaders
     * @return bool|string
     */
    protected static function responseJson (mixed $data, array $httpHeaders = []) : bool|string
    {
        header_remove('Set-Cookie');
        header('content-type: application/json');

        if (count($httpHeaders)) {
            foreach ($httpHeaders as $httpHeader) {
                header($httpHeader);
            }
        }

        return json_encode($data);
    }
}