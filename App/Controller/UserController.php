<?php

namespace App\Controller;
use App\Model\User;
use App\Repository\UserRepository;

class UserController extends Controller
{
    /**
     * @return bool|string
     */
    public function index (): bool|string
    {
        $data = (new User())->get("id", "first_name", "last_name");

        return self::responseJson([
            'message' => 'Successfully!',
            'user' => $data
        ]);
    }

    /**
     * @return bool|string
     */
    public function store (): bool|string
    {
        UserRepository::saveUser();
        return self::responseJson([
            'message' => 'Successfully!',
        ]);
    }
}