<?php

namespace App\Repository;

use App\Model\User;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @return void
     */
    public static function saveUser () {
        $user = new User();
        $user->create([
            'first_name' => $_POST['name'],
            'last_name' => $_POST['surname'],
        ]);
    }
}