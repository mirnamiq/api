<?php

namespace App\Repository;

interface UserRepositoryInterface
{
    /**
     * @return void
     */
    public static function saveUser ();
}