<?php

namespace App\Library;

interface RouteInterface
{
    /**
     * @param string $method
     * @param string $path
     * @param array $obj
     * @return void
     */
    public static function callMethod (string $method, string $path, array $obj);

    /**
     * @param string $path
     * @param array $obj
     * @return void
     */
    public static function get (string $path, array $obj);

    /**
     * @param string $path
     * @param array $obj
     * @return void
     */
    public static function post (string $path, array $obj);
}