<?php

namespace App\Library;

class Route implements RouteInterface {
    /**
     * @param string $path
     * @param array $obj
     * @return void
     */
    public static function get (string $path, array $obj) {
        self::callMethod('GET', $path, $obj);
    }

    /**
     * @param string $path
     * @param array $obj
     * @return void
     */
    public static function post (string $path, array $obj) {
        self::callMethod('POST', $path, $obj);
    }

    /**
     * @param string $method
     * @param string $path
     * @param array $obj
     * @return void
     */
    public static function callMethod(string $method, string $path, array $obj)
    {
        if($path === URI) {
            if($_SERVER['REQUEST_METHOD'] === $method) {
                exit((new $obj[0])->{$obj[1]}());
            }
            else {
                header('HTTP/1.1 405 Method Not Allowed');
                exit();
            }
        }
    }
}