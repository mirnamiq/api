<?php

namespace App\Library;

use PDO;

class DB extends PDO implements DBInterface
{
    public function __construct()
    {
        parent::__construct("pgsql:host=".DATABASE_HOST.";dbname=".DATABASE_NAME, DATABASE_USERNAME, DATABASE_PASSWORD);
        $this->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $this;
    }
}